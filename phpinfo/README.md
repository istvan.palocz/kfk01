Futtatás
-------

Mielőtt futtatod, buildeld le!
Mielőtt futtatod, szabd testre!

  cp docker-compose.override.yml.dev docker-compose.override.yml

Ha valami nem kerek állítsd be az overrid-ban!
Ha ezek megvannak add ki a következő parancsot:

  docker-compose up -d



BUILD
-----

Futtasd a következő parancsot:

  docker-compose build
