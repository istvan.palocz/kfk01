<?php 
require_once('../vendor/autoload.php');
use Symfony\Component\HttpFoundation\Response;

$response = new Response(
    'Hello World!!!',
    Response::HTTP_OK,
    ['content-type' => 'text/html']
);
$response->send();
