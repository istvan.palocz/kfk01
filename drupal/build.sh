docker rm -f drupal
docker build -t drupaltry .
docker run --rm -p 80:80 --name drupal -d -e DRUPAL_PROFILE=demo_umami drupaltry
docker exec -ti drupal bash
