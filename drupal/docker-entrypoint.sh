#!/bin/bash
if [ -z "$DRUPAL_DB_URL" ]; then
  export DRUPAL_DB_URL="sqlite:///dev/shm/db.sqlite"
fi

if [ -z "$DRUPAL_PROFILE" ]; then
  export DRUPAL_PROFILE="minimal"
fi

su --shell /bin/bash -c "drush si -y --db-url=$DRUPAL_DB_URL $DRUPAL_PROFILE" www-data

exec "$@"
